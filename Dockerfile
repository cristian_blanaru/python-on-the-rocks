# Use the official Python base image
FROM python:3.9-slim

# Set environment variables
ENV APP_USER=myuser
ENV APP_HOME=/home/$APP_USER

# Security
RUN groupadd -r $APP_USER && useradd -r -g $APP_USER $APP_USER

# Set the working directory in the container
WORKDIR $APP_HOME

# Copy the requirements.txt file and install dependencies
COPY requirements.txt .
RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

# Copy the Flask application code into the container
COPY app.py .
COPY test_app.py .

# Change ownership of the application directory to the non-root user
RUN chown -R $APP_USER:$APP_USER $APP_HOME

# Switch to the non-root user
USER $APP_USER

# Expose the port on which the Flask application will run
EXPOSE 5000

# Set the command to run the Flask application when the container starts
CMD ["python", "app.py"]