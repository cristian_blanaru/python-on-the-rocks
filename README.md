# Python on the Rocks

There is no better way to learn than by doing. The experience and insights you will gain will help you become better, smarter and wiser.

The project wants to build code in GitLab, run some terraform action and then also ensure that a helm chart is applied in a cluster.

JIC you need the ArgoCD deployment
```
project: default
source:
  repoURL: 'https://gitlab.com/cristian_blanaru/python-on-the-rocks.git'
  path: helm/pyotr
  targetRevision: HEAD
destination:
  server: 'https://kubernetes.default.svc'
  namespace: pyotr
syncPolicy:
  syncOptions:
    - CreateNamespace=true
```