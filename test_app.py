import pytest
import requests

@pytest.fixture
def base_url():
    return 'http://127.0.0.1:5000'

def test_hello_world(base_url):
    response = requests.get(f"{base_url}/")
    assert response.status_code == 200
    assert response.text == 'Hello, Cristian!'